var del			= require('del');
var file		= require('gulp-file');
var gvinyl		= require('glob-to-vinyl');
var gulp		= require('gulp');
var uglify		= require('gulp-uglify');
var minifyCSS	= require('gulp-minify-css');
var sass		= require('gulp-sass');
var exec		= require('child_process').exec;
var concat		= require("gulp-concat");


// HEADER JS
var headerjs	= [
	'modernizr.js',
	'jquery.js',
	'bootstrap.js',
	'twitter-widgets.js'
];


// FOOTER JS
var footerjs = [
	'main.js'
];

/**
 * ---------------
 * HELPERS
 * ---------------
 */
var time = function () {
	var d = new Date();
	return '[' + d.toLocaleTimeString().split(' ')[0] + '] -> ';
};

/**
 * ----------------
 * build CLEANUP
 * ---------------
 */

// TASK - cleanse: Removes all files from the build dir.
gulp.task('cleanse', function (cb) {
	del(['./build/**']);
	cb();
});


/**
 * -----------------
 * build PUBLIC JS
 * -----------------
 */

// TASK - jsmergeheader: Compiles headerjs[] into public/js/header.js.
gulp.task('jsmergeheader', function (cb) {
	var srcs = [];
	for (var i = 0; i < headerjs.length; i++) { srcs.push('./src/public/js/' + headerjs[i]); }

	gulp.src(srcs)
    .pipe(concat('header.js'))
    .pipe(gulp.dest('./src/public/js/'));

	console.log('---------------------------------------------------------------------------------------------');
	console.log(time() + ' updated: ./src/public/js/header.js');
	console.log('---------------------------------------------------------------------------------------------');

	cb();
});

// TASK - jsmergefooter: Compiles footerjs[] into public/js/footer.js.
gulp.task('jsmergefooter', function (cb) {
	var srcs = [];
	for (var i = 0; i < footerjs.length; i++) { srcs.push('./src/public/js/' + footerjs[i]); }

	gulp.src(srcs)
    .pipe(concat('footer.js'))
    .pipe(gulp.dest('./src/public/js/'));

	console.log('---------------------------------------------------------------------------------------------');
	console.log(time() + ' updated: ./src/public/js/footer.js');
	console.log('---------------------------------------------------------------------------------------------');

	cb();
});

// TASK - jsmerge: Compiles both the header.js and footer.js files
gulp.task('jsmerge', ['jsmergeheader', 'jsmergefooter']);


/**
 * -------------------
 * COPY TO build DIR
 * -------------------
 */

// TASK - build: Copies all the necessary files from the ../src dir to the ./build dir.
gulp.task('build', function (cb) {
	del(['./build/**'], function (err, deletedFiles) {
		// Copy & minify cloud .js files to build/cloud.
		gulp.src('./src/cloud/*.js')
		.pipe(uglify())
		.pipe(gulp.dest('./build/cloud/'));

		// Copy src/cloud/views to build/cloud/views.
		gulp.src('./src/cloud/views/**')
		.pipe(gulp.dest('./build/cloud/views/'));

		// Copy src/cloud/func to build/cloud/func.
		gulp.src('./src/cloud/func/**')
		.pipe(uglify())
		.pipe(gulp.dest('./build/cloud/func/'));

		// Copy src/config to build/config.
		gulp.src('./src/config/**')
		.pipe(gulp.dest('./build/config/'));

		// Copy & minify src/public/css/*.css to build/public/css.
		gulp.src('./src/public/css/*.css')
		.pipe(minifyCSS({keepBreaks:false}))
		.pipe(gulp.dest('./build/public/css/'));

		// Copy src/public/fonts to build/public/fonts.
		gulp.src('./src/public/fonts/**')
		.pipe(gulp.dest('./build/public/fonts/'));

		// Copy src/public/img to build/public/img.
		gulp.src('./src/public/img/**')
		.pipe(gulp.dest('./build/public/img/'));

		// Copy & minify src/public/js to build/public/js.
		gulp.src('./src/public/js/**')
		.pipe(uglify())
		.pipe(gulp.dest('./build/public/js/'));

		// Copy any file in the src/public folder.
		gulp.src(['./src/public/*.*', '!./src/public/temp*.html'])
		.pipe(gulp.dest('./build/public/'));

		cb();
	});
});


/**
 * -------
 * WATCH
 * -------
 */

// TASK - watch: Listens for specific file(s) changes.
gulp.task('watch', function (cb) {

	// WATCH - Was a public/js/*.js file changed?
	gulp.watch('./src/public/js/*.js', function (evt) {
		var fp	= evt.path.split('/');
		var f	= fp[fp.length - 1];

		// header file?
		if (headerjs.indexOf(f) > -1) {
			console.log(time() + evt.type + ': ' + evt.path);
			exec('gulp jsmergeheader');
			return;
		}

		// footer file?
		if (footerjs.indexOf(f) > -1) {
			console.log(time() + evt.type + ': ' + evt.path);
			exec('gulp jsmergefooter');
			return;
		}

		// Not a header or footer file?
		console.log(time() + evt.type + ': ' + evt.path);
		gulp.src(evt.path)
		.pipe(gulp.dest('./build/public/js/'));
	});

	// WATCH - Was a config file changed? -> replace the build folder's file
	gulp.watch('./src/config/*.json', function (evt) {
		console.log(time() + evt.type + ': ' + evt.path);
		gulp.src(evt.path)
		.pipe(gulp.dest('./build/cloud/config/'));
	});

	// WATCH - Was app.js changed? -> replace the build folder's app.js
    gulp.watch('./src/cloud/app.js', function (evt) {
       console.log(time() + evt.type + ': ' + evt.path);
       gulp.src(evt.path)
       .pipe(uglify())
       .pipe(gulp.dest('./build/cloud/'));
    });

    // WATCH - Was main.js changed? -> replace the build folder's app.js
    gulp.watch('./src/cloud/main.js', function (evt) {
       console.log(time() + evt.type + ': ' + evt.path);
       gulp.src(evt.path)
       .pipe(uglify())
       .pipe(gulp.dest('./build/cloud/'));
    });

	// WATCH - Was a cloud/func/*.js file changed? -> replace the build folder's file
	gulp.watch('./src/cloud/func/*.js', function (evt) {
		console.log(time() + evt.type + ': ' + evt.path);
		gulp.src(evt.path)
		.pipe(uglify())
		.pipe(gulp.dest('./build/cloud/func/'));
	});

	// WATCH - Was a cloud/views/*.ejs file changed? -> replace the build folder's file
	gulp.watch('./src/cloud/views/*.ejs', function (evt) {
		console.log(time() + evt.type + ': ' + evt.path);
		gulp.src(evt.path)
		.pipe(gulp.dest('./build/cloud/views/'));
	});

	// WATCH - Was a .css file changed? -> replace the build folder's file
	gulp.watch('./src/public/css/*.css', function (evt) {
		console.log(time() + evt.type + ': ' + evt.path);
		gulp.src(evt.path)
		.pipe(minifyCSS({keepBreaks:false}))
		.pipe(gulp.dest('./build/public/css/'));
	});

	cb();
});


/**
 * --------------
 * DEFAULT TASK
 * --------------
 */

gulp.task('default', function () { });


/**
 * -------------------------------------------------------------------------------------------
 * EOF
 * -------------------------------------------------------------------------------------------
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 * Why are you still reading this file?
 * EOF = END OF FILE!
 */