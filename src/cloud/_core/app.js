/**
 * ---------------------------------------
 * Required modules
 * ---------------------------------------
 */
var pehd 	= require("parse-express-https-redirect"),
    pecs    = require("parse-express-cookie-session"),
	express = require("express"),
    _       = require("underscore"),
    acfg    = require("cloud/_app/config.js");


/**
 * ---------------------------------------
 * Config files
 * ---------------------------------------
 */
 // Merge the core app file with the core config file
var config = require("cloud/_core/config.js");
for (var prop in acfg.config) { config[prop] = acfg.config[prop]; }

/**
 * ---------------------------------------
 * App setup
 * ---------------------------------------
 */
var app = express();
    app.set("views", "cloud");
    app.set("view engine", "ejs");
    app.set("x-powered-by", false);

    app.use(express.bodyParser());
    app.use(express.cookieParser("fd6c6375da872b"));
    app.use(pecs({cookie: {maxAge: 360000000}}));

var rendering = false;

/**
 * ---------------------------------------
 * Default Params
 * ---------------------------------------
 */
var defaultParams = {title: '404', meta: {title: '404', description: 'error', keywords: '404,error', protocol: 'http'}};

/**
 * ---------------------------------------
 * Hooks
 * ---------------------------------------
 */
/**
 * @function routeCallback();
 * @description Catch all route callback function that queries the Parse.Object("Page") table
 * for the requested url and returns the object"s values to use in the template.
 * @param {Object} request The request object.
 * @param {Object} response The response object.
 */
var routeCallback = function (request, response) {

    var path = request.params['path'] || request.route.path;
        path = (_.isEmpty(path)) ? '/' : path;
        path = (path == '*') ? request.path : path;

    // Get the page object
    Parse.Cloud.run("content_get", {column: "routes", find: path}, {
        success: function (page) {
            if (_.isEmpty(page)) {
                response.render(config.view['404'].template, config.view['404'].params); // render the 404 page
            } else {
                // Validate if the page is accessible via the url
                if (page.accessible == false && request.params['accessOverride'] != true) {
					response.render(config.view['404'].template, config.view['404'].params); // render the 404 page
				} else {
                    page.params['body']     = request.body;           // append the body variables
                    page.params['query']    = request.query;          // append the query variables
                    page.params['route']    = page.routes[0];         // append the route property
                    page.params['config']   = config;                 // append the config property
                    page.params['protocol'] = request.protocol;       // append the http protocol property

                    response.render(page.template, page.params);    // render the page
                }
            }
        },
        error: function (err) {
            response.render(config.view['404'].template, config.view['404'].params); // render the 404 page
        }
    });
};

/**
 * ---------------------------------------
 * Routes
 * ---------------------------------------
 */

// Homepage
app.all(['/'], routeCallback);

// Catch all
app.all('*', routeCallback);

/**
 * ---------------------------------------
 * Initialize App (must be last line!)
 * ---------------------------------------
 */
app.listen();