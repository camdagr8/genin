exports.config = {
	view: {
		"404": {
			template: "_core/views/404",
			params: {
				title: '404',
				meta: {
					title: '404',
					description: 'error',
					keywords: '404,error'
				}
			}
		}
	}
};